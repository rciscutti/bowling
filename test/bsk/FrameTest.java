package bsk;

import static org.junit.Assert.*;

import java.util.ArrayList;

import tdd.training.bsk.BowlingException;
import tdd.training.bsk.Frame;

import org.junit.Test;


public class FrameTest {

	@Test
	public void testSeePinsKnockedDownInFrame() throws Exception{
		Frame frame = new Frame(2,4);
		
		ArrayList<Integer> pinsChoosedForEachThrow = new ArrayList<>();
		pinsChoosedForEachThrow.add(2);
		pinsChoosedForEachThrow.add(4);
		
		ArrayList<Integer> givenPinsForEachThrow = new ArrayList<>();
		givenPinsForEachThrow.add(frame.getFirstThrow());
		givenPinsForEachThrow.add(frame.getSecondThrow());
		
		assertEquals(pinsChoosedForEachThrow,givenPinsForEachThrow);
	}
	
	@Test
	public void testCalculateFrameScore() throws Exception{
		Frame frame = new Frame(2,6);
		
		ArrayList<Integer> pinsChoosedForEachThrow = new ArrayList<>();
		pinsChoosedForEachThrow.add(2);
		pinsChoosedForEachThrow.add(6);
		int score = pinsChoosedForEachThrow.get(0) + pinsChoosedForEachThrow.get(1);
		
		ArrayList<Integer> givenPinsForEachThrow = new ArrayList<>();
		givenPinsForEachThrow.add(frame.getFirstThrow());
		givenPinsForEachThrow.add(frame.getSecondThrow());
		
		assertEquals(score, frame.getScore());
	}
	
	
	@Test(expected = BowlingException.class)
	public void testNegativeValueNotAcceptable() throws Exception{
		Frame frame = new Frame(-2,6);
	}
	
	@Test(expected = BowlingException.class)
	public void testValueGreaterThan10NotAcceptable() throws Exception{
		Frame frame = new Frame(11,6);
	}
	
	@Test(expected = BowlingException.class)
	public void testStrikeWithSecondThrowNotEqualTo0() throws Exception{
		Frame frame = new Frame(10,1);
	}
	
	@Test(expected = BowlingException.class)
	public void testPinsDownSumIsGreaterThan10() throws Exception{
		Frame frame = new Frame(10,1);
	}
	
	}
