package bsk;

import static org.junit.Assert.*;

import java.util.ArrayList;

import org.junit.Test;

import tdd.training.bsk.Frame;
import tdd.training.bsk.Game;

public class GameTest {

	@Test
	public void testCreateAndChooseGameFrame() throws Exception{
		Game game = new Game();
		ArrayList <Integer> givenFrameThrow = new ArrayList<>();
		ArrayList <Integer> choosedFrameThrow = new ArrayList<>();
		
		game.addFrame(new Frame(1,5));
		game.addFrame(new Frame(3,6));
		game.addFrame(new Frame(7,2));
		game.addFrame(new Frame(3,6));
		game.addFrame(new Frame(4,4));
		game.addFrame(new Frame(5,3));
		game.addFrame(new Frame(3,3));
		game.addFrame(new Frame(4,5));
		game.addFrame(new Frame(8,1));
		game.addFrame(new Frame(2,6));
		
		givenFrameThrow.add(game.getFrameAt(0).getFirstThrow());
		givenFrameThrow.add(game.getFrameAt(0).getSecondThrow());
		
		choosedFrameThrow.add(1);
		choosedFrameThrow.add(5);
		
		assertEquals(choosedFrameThrow, givenFrameThrow);
		
	}
	
	@Test
	public void testCalculateGameScore() throws Exception{
		Game game = new Game();
		
		game.addFrame(new Frame(1,5));
		game.addFrame(new Frame(3,6));
		game.addFrame(new Frame(7,2));
		game.addFrame(new Frame(3,6));
		game.addFrame(new Frame(4,4));
		game.addFrame(new Frame(5,3));
		game.addFrame(new Frame(3,3));
		game.addFrame(new Frame(4,5));
		game.addFrame(new Frame(8,1));
		game.addFrame(new Frame(2,6));
		
		assertEquals(81, game.calculateScore());
		
	}
	
	@Test
	public void testCalculateGameScoreWithSingleSpare() throws Exception{
		Game game = new Game();
		
		game.addFrame(new Frame(1,9));
		game.addFrame(new Frame(3,6));
		game.addFrame(new Frame(7,2));
		game.addFrame(new Frame(3,6));
		game.addFrame(new Frame(4,4));
		game.addFrame(new Frame(5,3));
		game.addFrame(new Frame(3,3));
		game.addFrame(new Frame(4,5));
		game.addFrame(new Frame(8,1));
		game.addFrame(new Frame(2,6));
		
		assertEquals(88, game.calculateScore());
		
	}
	
	@Test
	public void testCalculateGameScoreWithSingleStrike() throws Exception{
		Game game = new Game();
		
		game.addFrame(new Frame(10,0));
		game.addFrame(new Frame(3,6));
		game.addFrame(new Frame(7,2));
		game.addFrame(new Frame(3,6));
		game.addFrame(new Frame(4,4));
		game.addFrame(new Frame(5,3));
		game.addFrame(new Frame(3,3));
		game.addFrame(new Frame(4,5));
		game.addFrame(new Frame(8,1));
		game.addFrame(new Frame(2,6));
		
		assertEquals(94, game.calculateScore());
		
	}
	
	@Test
	public void testCalculateGameScoreWithSingleStrikeAndSingleSpare() throws Exception{
		Game game = new Game();
		
		game.addFrame(new Frame(10,0));
		game.addFrame(new Frame(4,6));
		game.addFrame(new Frame(7,2));
		game.addFrame(new Frame(3,6));
		game.addFrame(new Frame(4,4));
		game.addFrame(new Frame(5,3));
		game.addFrame(new Frame(3,3));
		game.addFrame(new Frame(4,5));
		game.addFrame(new Frame(8,1));
		game.addFrame(new Frame(2,6));
		
		assertEquals(103, game.calculateScore());
		
	}
	
	@Test
	public void testCalculateGameScoreWithMultipleStrikes() throws Exception{
		Game game = new Game();
		
		game.addFrame(new Frame(10,0));
		game.addFrame(new Frame(10,0));
		game.addFrame(new Frame(10,0));
		game.addFrame(new Frame(7,2));
		game.addFrame(new Frame(4,4));
		game.addFrame(new Frame(5,3));
		game.addFrame(new Frame(3,3));
		game.addFrame(new Frame(4,5));
		game.addFrame(new Frame(8,1));
		game.addFrame(new Frame(2,6));
		
		assertEquals(133, game.calculateScore());
		
	}
	
	@Test
	public void testCalculateGameScoreWithMultipleSpares() throws Exception{
		Game game = new Game();
		
		game.addFrame(new Frame(8,2));
		game.addFrame(new Frame(5,5));
		game.addFrame(new Frame(7,2));
		game.addFrame(new Frame(3,6));
		game.addFrame(new Frame(4,4));
		game.addFrame(new Frame(5,3));
		game.addFrame(new Frame(3,3));
		game.addFrame(new Frame(4,5));
		game.addFrame(new Frame(8,1));
		game.addFrame(new Frame(2,6));
		
		assertEquals(98, game.calculateScore());
		
	}
	
	@Test
	public void testCalculateGameScoreWithLastFrameSpare() throws Exception{
		Game game = new Game();
		game.setFirstBonusThrow(7);
		
		game.addFrame(new Frame(1,5));
		game.addFrame(new Frame(3,6));
		game.addFrame(new Frame(7,2));
		game.addFrame(new Frame(3,6));
		game.addFrame(new Frame(4,4));
		game.addFrame(new Frame(5,3));
		game.addFrame(new Frame(3,3));
		game.addFrame(new Frame(4,5));
		game.addFrame(new Frame(8,1));
		game.addFrame(new Frame(2,8));
		
		assertEquals(90, game.calculateScore());
		
	}
	
	@Test
	public void testCalculateGameScoreWithLastFrameStrike() throws Exception{
		Game game = new Game();
		game.setFirstBonusThrow(7);
		game.setSecondBonusThrow(2);
		
		game.addFrame(new Frame(1,5));
		game.addFrame(new Frame(3,6));
		game.addFrame(new Frame(7,2));
		game.addFrame(new Frame(3,6));
		game.addFrame(new Frame(4,4));
		game.addFrame(new Frame(5,3));
		game.addFrame(new Frame(3,3));
		game.addFrame(new Frame(4,5));
		game.addFrame(new Frame(8,1));
		game.addFrame(new Frame(10,0));
		
		assertEquals(92, game.calculateScore());
		
	}
	
	@Test
	public void testCalculateGameBestScore() throws Exception{
		Game game = new Game();
		game.setFirstBonusThrow(10);
		game.setSecondBonusThrow(10);
		
		game.addFrame(new Frame(10,0));
		game.addFrame(new Frame(10,0));
		game.addFrame(new Frame(10,0));
		game.addFrame(new Frame(10,0));
		game.addFrame(new Frame(10,0));
		game.addFrame(new Frame(10,0));
		game.addFrame(new Frame(10,0));
		game.addFrame(new Frame(10,0));
		game.addFrame(new Frame(10,0));
		game.addFrame(new Frame(10,0));
		
		assertEquals(300, game.calculateScore());
		
	}
}
