package tdd.training.bsk;

import java.util.ArrayList;


public class Game {
	ArrayList<Frame> game;
	private int firstBonusThrow = 0;
	private int secondBonusThrow = 0;
	
	/**
	 * It initializes an empty bowling game.
	 */
	public Game() {
		game = new ArrayList<>();
	}

	/**
	 * It adds a frame to this game.
	 * 
	 * @param frame The frame.
	 * @throws BowlingException
	 */
	public void addFrame(Frame frame) throws BowlingException {
		game.add(frame);
	}

	/**
	 * It return the i-th frame of this game.
	 * 
	 * @param index The index (ranging from 0 to 9) of the frame.
	 * @return The i-th frame.
	 * @throws BowlingException
	 */
	public Frame getFrameAt(int index) throws BowlingException {
		return game.get(index);	
	}

	/**
	 * It sets the first bonus throw of this game.
	 * 
	 * @param firstBonusThrow The first bonus throw.
	 * @throws BowlingException
	 */
	public void setFirstBonusThrow(int firstBonusThrow) throws BowlingException {
		this.firstBonusThrow = firstBonusThrow;
	}

	/**
	 * It sets the second bonus throw of this game.
	 * 
	 * @param secondBonusThrow The second bonus throw.
	 * @throws BowlingException
	 */
	public void setSecondBonusThrow(int secondBonusThrow) throws BowlingException {
		this.secondBonusThrow = secondBonusThrow;
	}

	/**
	 * It returns the first bonus throw of this game.
	 * 
	 * @return The first bonus throw.
	 */
	public int getFirstBonusThrow() {
		return firstBonusThrow;
	}

	/**
	 * It returns the second bonus throw of this game.
	 * 
	 * @return The second bonus throw.
	 */
	public int getSecondBonusThrow() {
		return secondBonusThrow;
	}

	/**
	 * It returns the score of this game.
	 * 
	 * @return The score of this game.
	 * @throws BowlingException
	 */
	public int calculateScore() throws BowlingException {
			int gameScore=0;
			
			for(int i=0; i<game.size(); i++) {
				
				if(getFrameAt(i).isSpare()) {
					calculateSpareBonus(i);
				
				}else if(getFrameAt(i).isStrike()){
						calculateStrikeBonus(i);
						
				}else{
					getFrameAt(i).setBonus(0);
				}
				
				gameScore += getFrameAt(i).getScore();
			}
			
		return gameScore;	
	}
	
	private void calculateStrikeBonus(int i) throws BowlingException {
		
		if(i == game.size()-1) {
			getFrameAt(i).setBonus(getFirstBonusThrow() + getSecondBonusThrow());
			
		}else if(i == game.size()-2){
			getFrameAt(i).setBonus(getFrameAt(i+1).getFirstThrow()+ getFirstBonusThrow());
			
		}else if(getFrameAt(i+1).isStrike() && i!=game.size()-1) {
				//Multiple Strike
				getFrameAt(i).setBonus(getFrameAt(i+1).getFirstThrow()+ getFrameAt(i+2).getFirstThrow());
				
			}else {
				//Single Strike
				getFrameAt(i).setBonus(getFrameAt(i+1).getFirstThrow() + getFrameAt(i+1).getSecondThrow());
			}
		
		
	}
	
	private void calculateSpareBonus(int i) throws BowlingException {
		if(i == game.size()-1) {
			getFrameAt(i).setBonus(getFirstBonusThrow());
		}else {
			getFrameAt(i).setBonus(getFrameAt(i+1).getFirstThrow());
		}
	}
	
}
